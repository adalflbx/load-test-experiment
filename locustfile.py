from locust import HttpUser, task

class User(HttpUser):
    @task
    def call_product_flow_1(self):
        self.client.get("https://gateway.flowbox.me/public/feed/7SzV2XvNTDOqcE88pMQrww?cursor=null&postsPerPage=29&productId=7&device=a6d46690-8bfc-b746-318e-4b3a450f89fa")

    # @task
    # def call_product_flow_2(self):
    #     self.client.get("https://gateway.flowbox.me/public/feed/R5Rj3GodSCWFhfFXbXrJmA\?cursor\=null\&postsPerPage\=29\&productId\=5\&device\=a6d46690-8bfc-b746-318e-4b3a450f89fa")

    # @task
    # def call_tag_flow_1(self):
    #     self.client.get("https://gateway.flowbox.me/public/feed/7SzV2XvNTDOqcE88pMQrww\&tags\=chess")
